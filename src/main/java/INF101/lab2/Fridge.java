package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	
	ArrayList<FridgeItem> foodInFridge = new ArrayList<FridgeItem>();

	@Override
	public int nItemsInFridge() {
		return foodInFridge.size();
	}

	
	int max_itemFridge = 20;
	@Override
	public int totalSize() {
		return max_itemFridge;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if(nItemsInFridge()<totalSize()) {
			foodInFridge.add(item);
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public void takeOut(FridgeItem item) throws NoSuchElementException {
		if(nItemsInFridge()==0){
		     throw new NoSuchElementException();
		}
		else{ 
		    	foodInFridge.remove(item);  
		} 
	}

	@Override
	public void emptyFridge() {
		foodInFridge.clear();

	}

	ArrayList<FridgeItem> listOfExpiredItems = new ArrayList<FridgeItem>();
	//int size = nItemsInFridge();
	
	@Override
	public List<FridgeItem> removeExpiredFood() {
		//loopa igjennom alle elementa i foodInFridge og tar dei som er utgått i lista listOfExpiredItems
		int size = nItemsInFridge();
		for(int i = 0; i<size; i=i+1) {
			FridgeItem item = foodInFridge.get(i);
			if (item.hasExpired()) {
				listOfExpiredItems.add(item);
			}
			
		}
		//sletter utgåtte varer fra listen foodInFridge
		for(int i = 0; i<listOfExpiredItems.size(); i++) {
			takeOut(listOfExpiredItems.get(i));
		}
		return listOfExpiredItems;
		
       

       
	}

        

	
}
